import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { IntroPage } from '../pages/intropage/intropage';

import { Berpergian } from '../pages/berpergian/berpergian';
import { Perjalanan } from '../pages/perjalanan/perjalanan';
import { Result } from '../pages/result/result';
import { Pemesanan } from '../pages/pemesanan/pemesanan';
import { Detail } from '../pages/detail/detail';
import { DetailContact } from '../pages/detail-contact/detail-contact';
import { Travel } from '../pages/travel-partner/travel-partner';
import { CaraKerja } from '../pages/cara-kerja/cara-kerja';
import { SiapaKami } from '../pages/siapa-kami/siapa-kami';
import { Hubungi } from '../pages/hubungi-kami/hubungi-kami';
import { Kebijakan } from '../pages/kebijakan-privacy/kebijakan-privacy';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    IntroPage,
    HomePage,
    ListPage, 
    Berpergian,
    Perjalanan,
    Result,
    Pemesanan,
    Detail,
    DetailContact,
    Travel,
    CaraKerja,
    SiapaKami,
    Hubungi,
    Kebijakan
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    IntroPage,
    HomePage,
    ListPage,
    Berpergian,
    Perjalanan,
    Result,
    Pemesanan,
    Detail,
    DetailContact,
    Travel,
    CaraKerja,
    SiapaKami,
    Hubungi,
    Kebijakan
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
