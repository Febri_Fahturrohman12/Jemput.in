import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { IntroPage } from '../pages/intropage/intropage';

import { Berpergian } from '../pages/berpergian/berpergian';
import { Perjalanan } from '../pages/perjalanan/perjalanan';
import { Result } from '../pages/result/result';
import { Pemesanan } from '../pages/pemesanan/pemesanan';
import { Detail } from '../pages/detail/detail';
import { DetailContact } from '../pages/detail-contact/detail-contact';
import { Travel } from '../pages/travel-partner/travel-partner';
import { CaraKerja } from '../pages/cara-kerja/cara-kerja';
import { SiapaKami } from '../pages/siapa-kami/siapa-kami';
import { Hubungi } from '../pages/hubungi-kami/hubungi-kami';
import { Kebijakan } from '../pages/kebijakan-privacy/kebijakan-privacy';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

    rootPage: any = IntroPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'IntroPage', component: IntroPage },
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage },

      { title: 'Berpergian', component: Berpergian },
      { title: 'Perjalanan', component: Perjalanan },
      { title: 'Result', component: Result },
      { title: 'Pemesanan', component: Pemesanan },
      { title: 'Detail', component: Detail },
      { title: 'DetailContact', component: DetailContact },
      { title: 'Travel', component: Travel },
      { title: 'CaraKerja', component: CaraKerja },
      { title: 'SiapaKami', component: SiapaKami },
      { title: 'Hubungi', component: Hubungi },
      { title: 'Kebijakan', component: Kebijakan }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

}
