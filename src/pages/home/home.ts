import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	ImageArray: any =[];
  constructor(public navCtrl: NavController) {
  	this.ImageArray = [
  	{'image':'../assets/imgs/slide_01.png'},
  	{'image':'../assets/imgs/slide_02.png'},
  	{'image':'../assets/imgs/slide_03.png'}
  	]
  }

}