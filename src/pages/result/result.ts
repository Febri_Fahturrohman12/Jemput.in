import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-result',
  templateUrl: 'result.html'
})
export class Result {

  constructor(public navCtrl: NavController) {

  }

}