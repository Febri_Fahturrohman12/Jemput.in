import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-siapa-kami',
  templateUrl: 'siapa-kami.html'
})
export class SiapaKami {

  constructor(public navCtrl: NavController) {

  }

}