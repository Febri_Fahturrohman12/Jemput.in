import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-detail-contact',
  templateUrl: 'detail-contact.html'
})
export class DetailContact {

  constructor(public navCtrl: NavController) {

  }

}