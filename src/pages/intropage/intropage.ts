import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-intropage',
  templateUrl: 'intropage.html'
})
export class IntroPage {
  ImageArray: any =[];
  constructor(public navCtrl: NavController) {
    this.ImageArray = [
    {'image':'../assets/imgs/slide_01.png'},
    {'image':'../assets/imgs/slide_02.png'},
    {'image':'../assets/imgs/slide_03.png'}
    ]
  }
  toHome(){
  this.navCtrl.push(HomePage);
  }

}
